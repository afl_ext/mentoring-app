Feature: Creating new accounts
  In order to create a new user account
  As an anonymous user
  Having a valid registration token
  I need to be able to create a new user account

  Scenario: Creating a new user account
    Given there exists a registration token "TOKEN01"
    When I navigate to "/register"
    And I send "POST" request to "/register" with data:
      | key                                  | value        |
      | registration_form[businessName]      | abc          |
      | registration_form[vinNumber]         | 12345678     |
      | registration_form[email]             | abc@def.pl   |
      | registration_form[foundationYear]    | 2017         |
      | registration_form[annualIncome]     | 1000         |
      | registration_form[washMoney]         | 1            |
      | registration_form[shareNSATax]       | 1            |
      | registration_form[registrationToken] | VE9LRU4wMQ== |
    Then the response code is 200
    And the response contains "successful"

  Scenario: Trying to create a new user account with bad token
    When I navigate to "/register"
    And I send "POST" request to "/register" with data:
      | key                                  | value        |
      | registration_form[businessName]      | abc          |
      | registration_form[vinNumber]         | 12345678     |
      | registration_form[email]             | abc@def.pl   |
      | registration_form[foundationYear]    | 2017         |
      | registration_form[annualIncome]     | 1000         |
      | registration_form[washMoney]         | 1            |
      | registration_form[shareNSATax]       | 1            |
      | registration_form[registrationToken] | VE9LRU4wMQ== |
    Then the response code is 400

  Scenario: Trying to create a new user account with negative income
    Given there exists a registration token "TOKEN01"
    When I navigate to "/register"
    And I send "POST" request to "/register" with data:
      | key                                  | value        |
      | registration_form[businessName]      | abc          |
      | registration_form[vinNumber]         | 12345678     |
      | registration_form[email]             | abc@def.pl   |
      | registration_form[foundationYear]    | 2017         |
      | registration_form[annualIncome]     | 0            |
      | registration_form[washMoney]         | 1            |
      | registration_form[shareNSATax]       | 1            |
      | registration_form[registrationToken] | VE9LRU4wMQ== |
    Then the response code is 400

  Scenario: Trying to create a new user account with invalid email
    Given there exists a registration token "TOKEN01"
    When I navigate to "/register"
    And I send "POST" request to "/register" with data:
      | key                                  | value        |
      | registration_form[businessName]      | abc          |
      | registration_form[vinNumber]         | 12345678     |
      | registration_form[email]             | abcdef.pl    |
      | registration_form[foundationYear]    | 2017         |
      | registration_form[annualIncome]     | 0            |
      | registration_form[washMoney]         | 1            |
      | registration_form[shareNSATax]       | 1            |
      | registration_form[registrationToken] | VE9LRU4wMQ== |
    Then the response code is 400
