Feature: Logging in with login token
  In order to log in
  As an anonymous user
  Having a valid login token
  I need to be able to log in using API Zone login form

  Scenario: Logging in with valid login token
    Given there exists a user with login token "TOKEN01"
    When I navigate to "/login-api-zone"
    And I send asynchronous "POST" request to "/login-api-zone-check" with data:
      | key    | value   |
      | apikey | TOKEN01 |
    Then the response code is 200
    And the response contains "true"
    Then I navigate to "/api-zone"
    Then the response code is 200
    And the response contains "Log out"

  Scenario: Logging in with invalid login token
    Given there exists a user with login token "TOKEN01"
    When I navigate to "/login-api-zone"
    And I send asynchronous "POST" request to "/login-api-zone-check" with data:
      | key    | value   |
      | apikey | TOKEN02 |
    Then the response code is 400
    And the response contains "false"
    Then I navigate to "/api-zone"
    Then the response code is 302
