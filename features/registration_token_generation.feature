Feature: Generating registration tests
  In order to generate new registration token
  As an administrator
  Using the CLI
  I need to be able to generate a new registration token with a command


  Scenario: Checking generation of new registration token
    When I execute new registration token command
    Then the response contains the word "OK"
    And the response contains a valid token


  Scenario: Checking if the system refuses to generate tokens when exceeding limits
    When I generate maximum allowed count of tokens
    Then no exception was thrown
    Then I generate 1 tokens
    Then exception was thrown