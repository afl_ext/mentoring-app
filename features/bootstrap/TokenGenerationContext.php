<?php
declare(strict_types=1);

use App\Command\CreateRegistrationTokenCommand;
use App\Service\RegistrationTokenGeneratorInterface;
use App\Service\RegistrationTokenStorageInterface;
use App\Service\TokenSerializerInterface;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use PHPUnit\Framework\Assert;
use Doctrine\ORM\EntityManagerInterface;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;

/**
 * Defines application features from the specific context.
 */
class TokenGenerationContext extends RawMinkContext implements Context
{
    use KernelDictionary;

    /**
     * @var string
     */
    private $lastResponseContents = "";
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var bool
     */
    private $wasLimitExceededExceptionThrown = false;
    /**
     * @var int
     */
    private $tokenLimit;
    /**
     * @var int
     */
    private $tokenTimeout;
    /**
     * @var RegistrationTokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var RegistrationTokenGeneratorInterface
     */
    private $tokenGenerator;
    /**
     * @var TokenSerializerInterface
     */
    private $tokenSerializer;
    /**
     * @var CreateRegistrationTokenCommand
     */
    private $creationCommand;

    /**
     * @param KernelInterface $kernel
     * @param RegistrationTokenStorageInterface $tokenStorage
     * @param RegistrationTokenGeneratorInterface $tokenGenerator
     * @param TokenSerializerInterface $tokenSerializer
     * @param CreateRegistrationTokenCommand $creationCommand
     * @param EntityManagerInterface $entityManager
     * @param int $tokenLimit
     * @param int $tokenTimeout
     */
    public function __construct(KernelInterface $kernel, RegistrationTokenStorageInterface $tokenStorage,
                                RegistrationTokenGeneratorInterface $tokenGenerator, TokenSerializerInterface $tokenSerializer,
                                CreateRegistrationTokenCommand $creationCommand, EntityManagerInterface $entityManager,
                                int $tokenLimit, int $tokenTimeout)
    {
        $this->tokenLimit = $tokenLimit;
        $this->tokenTimeout = $tokenTimeout;
        $this->kernel = $kernel;
        $this->tokenStorage = $tokenStorage;
        $this->tokenGenerator = $tokenGenerator;
        $this->tokenSerializer = $tokenSerializer;
        $this->creationCommand = $creationCommand;
        $this->entityManager = $entityManager;
    }

    /**
     * @BeforeScenario
     * @param BeforeScenarioScope $scope
     */
    public function theDatabaseIsCleared(BeforeScenarioScope $scope): void
    {
        $purger = new ORMPurger($this->entityManager);
        $purger->purge();
        $this->wasLimitExceededExceptionThrown = false;
    }

    /**
     * @When I execute new registration token command
     */
    public function iExecuteNewRegistrationTokenCommand(): void
    {
        $stringInput = new StringInput('');
        $buferedOutput = new BufferedOutput();
        $this->creationCommand->run($stringInput, $buferedOutput);
        $this->lastResponseContents = trim($buferedOutput->fetch());
    }

    /**
     * @Then the response contains the word :arg1
     * @param string $arg1
     */
    public function theResponseContainsTheWord(string $arg1): void
    {
        Assert::assertContains($arg1, $this->lastResponseContents);
    }

    /**
     * @Then the response contains a valid token
     */
    public function theResponseContainsAValidToken(): void
    {
        $exploded = explode(' ', $this->lastResponseContents); // ...
        $lastWord = trim($exploded[count($exploded) - 1]);
        $deserializedToken = $this->tokenSerializer->reverseTransform($lastWord);
        $tokenActive = $this->tokenStorage->isActiveToken($deserializedToken);
        Assert::assertTrue($tokenActive);
    }

    /**
     * @When I generate :arg1 tokens
     * @param int $tokensCount
     */
    public function iGenerateTokens(int $tokensCount): void
    {
        try {
            for ($tokens = 0; $tokens < $tokensCount; $tokens++) {
                $this->tokenGenerator->generateNewToken();
            }
        } catch (\App\Exception\RegistrationTokenLimitExceededException $e){
            $this->wasLimitExceededExceptionThrown = true;
        }
    }

    /**
     * @Then no exception was thrown
     */
    public function noExceptionWasThrown(): void
    {
        Assert::assertFalse($this->wasLimitExceededExceptionThrown);
    }

    /**
     * @Then exception was thrown
     */
    public function exceptionWasThrown(): void
    {
        Assert::assertTrue($this->wasLimitExceededExceptionThrown);
    }

    /**
     * @When I generate maximum allowed count of tokens
     */
    public function iGenerateMaximumAllowedCountOfTokens(): void
    {
        $this->iGenerateTokens($this->tokenLimit);
    }
}
