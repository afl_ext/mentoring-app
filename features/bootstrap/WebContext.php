<?php
declare(strict_types=1);

use Behat\Gherkin\Node\PyStringNode;
use App\Entity\User;
use App\Service\RegistrationTokenStorageInterface;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\FrameworkBundle\Client;
use PHPUnit\Framework\Assert;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

/**
 */
class WebContext extends RawMinkContext implements Context
{
    use KernelDictionary;

    /**
     * @var Client
     */
    private $client;
    /**
     * @var RegistrationTokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param KernelInterface $kernel
     * @param RegistrationTokenStorageInterface $tokenStorage
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(KernelInterface $kernel, RegistrationTokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager)
    {
        $this->kernel = $kernel;
        $this->client = $this->getKernel()->getContainer()->get('test.client');
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    /**
     * @BeforeScenario
     * @param BeforeScenarioScope $scope
     */
    public function theDatabaseIsCleared(BeforeScenarioScope $scope): void
    {
        $purger = new ORMPurger($this->entityManager);
        $purger->purge();
    }

    /**
     * @Given there exists a registration token :arg1
     * @param $arg1
     */
    public function thereExistsARegistrationToken($arg1)
    {
        $this->tokenStorage->saveRegistrationToken($arg1);
    }

    /**
     * @When I send :arg1 request to :arg2 with data:
     * @param $arg1
     * @param $arg2
     * @param TableNode $table
     */
    public function iSendRequestToWithData($arg1, $arg2, TableNode $table)
    {
        $form = $this->client->getCrawler()->selectButton('Submit')->form();

        $rawTableNode = $table->getHash();
        foreach ($rawTableNode as $row){
            $form[$row['key']] = $row['value'];
        }
        $this->client->submit($form);
    }

    /**
     * @Then the response code is :arg1
     * @param $arg1
     */
    public function theResponseCodeIs($arg1)
    {
        $response = $this->client->getResponse();
        Assert::assertEquals($response->getStatusCode(), (int)$arg1);
    }

    /**
     * @Then the response contains :arg1
     * @param $arg1
     */
    public function theResponseContains($arg1)
    {
        $response = $this->client->getResponse();
        Assert::assertContains($arg1, $response->getContent());
    }

    /**
     * @When I navigate to :arg1
     * @param $arg1
     */
    public function iNavigateTo($arg1)
    {
        $this->client->request("GET", $arg1);
    }

    /**
     * @Given there exists a user with login token :arg1
     */
    public function thereExistsAUserWithLoginToken($arg1)
    {
        $user = new User($arg1, "", "", "", 0, 0, false, false, "");
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @When I send asynchronous :arg1 request to :arg2 with data:
     */
    public function iSendAsynchronousRequestToWithData($arg1, $arg2, TableNode $table)
    {
        $data = [];
        $rawTableNode = $table->getHash();
        foreach ($rawTableNode as $row){
            $data[$row['key']] = $row['value'];
        }
        $this->client->request($arg1, $arg2, $data);
    }
}
