<?php
declare(strict_types=1);
namespace App\Exception;


/**
 * It is thrown when a token limit is exceeded to signal this situation
 */
class RegistrationTokenLimitExceededException extends \Exception
{

}