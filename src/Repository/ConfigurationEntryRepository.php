<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\ConfigurationEntry;
use App\Service\KeyValueStoreInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ConfigurationEntryRepository
 * This class has some helper functions to manage runtime application configuration
 * It works as key val store with strings only
 */
class ConfigurationEntryRepository extends ServiceEntityRepository implements KeyValueStoreInterface
{
    /**
     * ConfigurationEntryRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ConfigurationEntry::class);
    }


    /**
     * Get a value by string key or null if not found
     * @param $key
     * @return string
     */
    public function getValueByKey(string $key): ?string
    {
        try {
            $configEntry = $this->createQueryBuilder('c')
                ->where('c.key = :key')->setParameter('key', $key)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            // the key field has an unique constrain
            return null;
        }
        if(null === $configEntry){
            return null;
        } else {
            return $configEntry->getValue();
        }
    }

    /**
     * Get an entity by string key or null if not found
     * @param $key
     * @return ConfigurationEntry|null
     */
    private function getEntityByKey(string $key) : ?ConfigurationEntry
    {
        try {
            return $this->createQueryBuilder('c')
                ->where('c.key = :key')->setParameter('key', $key)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            // the key field has an unique constrain
            return null;
        }
    }

    /**
     * Set a string value by string key
     * @param $key
     * @param $value
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setValueByKey(string $key, string $value): void
    {
        $configEntry = $this->getEntityByKey($key);
        if(null === $configEntry) {
            $configEntry = new ConfigurationEntry();
            $configEntry->setKey($key);
        }
        $configEntry->setValue($value);
        $em = $this->getEntityManager();
        $em->persist($configEntry);
        $em->flush();
    }

}
