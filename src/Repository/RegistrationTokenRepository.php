<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\RegistrationToken;
use App\Service\RegistrationTokenCountPerDayProviderInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

class RegistrationTokenRepository extends ServiceEntityRepository implements RegistrationTokenCountPerDayProviderInterface
{
    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RegistrationToken::class);
    }


    /**
     * Find a token by its raw representation
     * @param string $token
     * @return RegistrationToken|null
     */
    public function findByToken(string $token): ?RegistrationToken
    {
        try {
            return $this->createQueryBuilder('r')
                ->where('r.token = :value')->setParameter('value', $token)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * Gets token count generated since midnight
     * This is not used for token limit counter, but for a part of the token itself (different concept)
     * @return int
     */
    public function getTokenCountGeneratedToday(): int
    {
        $today = new \DateTime();
        $today->setTime(0,0);
        $timestamp = $today->getTimestamp();

        $qb = $this->createQueryBuilder('t');
        try {
            return (int)$qb->select($qb->expr()->count('t.id'))
                ->where('t.createTime > :midnight')
                ->setParameter('midnight', $timestamp)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            // there is no way for count select to be not unique
        }
    }

    /**
     * In this case the function does nothing because the count is extracted from database
     */
    public function registerNewToken(): void
    {
    }

}
