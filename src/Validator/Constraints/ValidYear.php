<?php
declare(strict_types=1);

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidYear extends Constraint
{
    /**
     * @var string
     */
    public $message = 'VALIDATOR.YEAR_NOT_VALID_INFORMATION';
}
