<?php
declare(strict_types=1);

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidRegistrationToken extends Constraint
{
    /**
     * @var string
     */
    public $message = 'VALIDATOR.REGISTRATION_TOKEN_NOT_VALID_INFORMATION';
}
