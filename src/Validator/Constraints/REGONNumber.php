<?php
declare(strict_types=1);

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class REGONNumber extends Constraint
{
    /**
     * @var string
     */
    public $message = 'VALIDATOR.REGON_NOT_VALID';
}
