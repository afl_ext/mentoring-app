<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use App\Service\RegistrationTokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class REGONNumberValidator extends ConstraintValidator
{

    private function isValidREGON($str)
    {
        if (strlen($str) != 9)
        {
            return false;
        }

        $arrSteps = array(8, 9, 2, 3, 4, 5, 6, 7);
        $intSum=0;
        for ($i = 0; $i < 8; $i++)
        {
            $intSum += $arrSteps[$i] * $str[$i];
        }
        $int = $intSum % 11;
        $intControlNr=($int == 10)?0:$int;
        if ($intControlNr == $str[8])
        {
            return true;
        }
        return false;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        if (!$this->isValidREGON($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}