<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use App\Service\RegistrationTokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


/**
 * Validates a year if it is not empty
 */
class ValidYearValidator extends ConstraintValidator
{

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (null !== $value && (int)$value > (int)date('Y')) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}