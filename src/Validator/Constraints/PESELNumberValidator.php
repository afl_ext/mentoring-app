<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use App\Service\RegistrationTokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class PESELNumberValidator extends ConstraintValidator
{

    private function isValidPESEL($str)
    {
        if (!preg_match('/^[0-9]{11}$/', $str)) {
            return false;
        }

        $arrSteps = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);
        $intSum = 0;
        for ($i = 0; $i < 10; $i++) {
            $intSum += $arrSteps[$i] * $str[$i];
        }
        $int = 10 - $intSum % 10;
        $intControlNr = ($int == 10) ? 0 : $int;
        if ($intControlNr == $str[10]) {
            return true;
        }
        return false;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        if (!$this->isValidPESEL($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}