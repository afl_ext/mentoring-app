<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use App\Service\RegistrationTokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


/**
 * Validates a registration token using a registration token storage interface
 */
class ValidRegistrationTokenValidator extends ConstraintValidator
{
    /**
     * @var RegistrationTokenStorageInterface
     */
    private $registrationTokenStorage;

    /**
     * @param RegistrationTokenStorageInterface $registrationTokenStorage
     */
    public function __construct(RegistrationTokenStorageInterface $registrationTokenStorage)
    {
        $this->registrationTokenStorage = $registrationTokenStorage;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$this->registrationTokenStorage->isActiveToken($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}