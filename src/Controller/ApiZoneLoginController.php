<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\ApiKeyLoginForm;
use App\Form\ApiTokenData;
use App\Form\RegistrationForm;
use App\Form\UserData;
use App\Repository\UserRepository;
use App\Security\ApiKeyAuthenticator;
use App\Security\User\UserProvider;
use App\Service\RegistrationTokenStorageInterface;
use App\Service\TokenSerializerInterface;
use App\Service\UserSearcherByLoginToken;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Authentication\Provider\PreAuthenticatedAuthenticationProvider;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\PlaintextPasswordEncoder;
use Symfony\Component\Security\Core\User\UserChecker;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Displays the login for asking for api key
 */
class ApiZoneLoginController extends Controller
{
    /**
     * @return Response
     */
    public function index()
    {
        $apiTokenData = new ApiTokenData();
        $form = $this->createForm(ApiKeyLoginForm::class, $apiTokenData);

        return $this->render('apiZoneLogin.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @param UserProvider $userProvider
     * @param ApiKeyAuthenticator $authenticator
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function check(Request $request, UserProvider $userProvider,
                          ApiKeyAuthenticator $authenticator, User $user = null)
    {
        if(null !== $user) {
            $unauthenticatedToken = new UsernamePasswordToken(
                $user,
                $user->getLoginToken(),
                'secured_area'
            );

            $token = $authenticator->authenticateToken($unauthenticatedToken, $userProvider, 'secured_area');

            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));

            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

            return $this->json(true, Response::HTTP_OK);
        } else {
            return $this->json(false, Response::HTTP_BAD_REQUEST);
        }
    }
}
