<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationForm;
use App\Form\UserData;
use App\Form\UserEditData;
use App\Form\UserEditForm;
use App\Service\RegistrationTokenStorageInterface;
use App\Service\TokenSerializerInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * Displays API Zone user data edit form
 */
class ApiZoneController extends Controller
{
    const TEMPLATE = 'apiZoneEditData.html.twig';

    public function __invoke(Request $request, UserInterface $user)
    {
        $userData = new UserEditData();
        $userData->loadFromUser($user);
        $form = $this->createForm(UserEditForm::class, $userData);
        $form->add('saveButton', SubmitType::class, array('label' => 'CORE.SUBMIT', 'attr' => ['class' => 'btn btn-primary']));
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if (!$form->isValid()) {
                $response = $this->render(static::TEMPLATE, ['form' => $form->createView()]);
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
                return $response;
            }

            $user->loadFromUserEditData($userData);

            $em = $this->getDoctrine()->getManager();
            $em->merge($user);
            $em->flush();

        }
        return $this->render(static::TEMPLATE, ['form' => $form->createView()]);
    }
}
