<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationForm;
use App\Form\UserData;
use App\Service\RegistrationTokenStorageInterface;
use App\Service\TokenSerializerInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * Registration controller, it displays registration form
 */
class RegistrationController extends Controller
{
    const REGISTRATION_TEMPLATE = 'registration.html.twig';
    const REGISTRATION_SUCCESSFUL_TEMPLATE = 'registrationSuccessful.html.twig';

    public function index(Request $request, RegistrationTokenStorageInterface $tokenStorage)
    {
        $userData = new UserData();
        $form = $this->createForm(RegistrationForm::class, $userData);
        $form->add('saveButton', SubmitType::class, array('label' => 'CORE.SUBMIT', 'attr' => ['class' => 'btn btn-primary']));
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if (!$form->isValid()) {
                $response = $this->render(static::REGISTRATION_TEMPLATE, ['form' => $form->createView()]);
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
                return $response;
            }

            $loginToken = $this->generateLoginToken();
            $tokenString = $userData->registrationToken;
            $user = new User($loginToken, $userData->businessName, strval($userData->vinNumber), $userData->email, (int)$userData->foundationYear,
                $userData->annualIncome, $userData->shareNSATax, $userData->washMoney, $tokenString);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $tokenStorage->deactivateToken($tokenString);
            $em->flush();

            return $this->render(static::REGISTRATION_SUCCESSFUL_TEMPLATE, ['loginToken' => $loginToken]);
        }
        return $this->render(static::REGISTRATION_TEMPLATE, ['form' => $form->createView(), 'errors' => []]);
    }

    private function generateLoginToken(): string
    {
        return Uuid::uuid4()->toString();
    }
}
