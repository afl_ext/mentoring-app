<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\CreditorProfileData;
use App\Form\LeasingDetailsData;
use App\Form\LeasingDetailsForm;
use App\Form\LeasingTakerProfileData;
use App\Form\LeasingTakerProfileForm;
use App\Form\MultipleCreditorProfilesData;
use App\Form\MultipleCreditorProfilesForm;
use App\Form\RegistrationForm;
use App\Form\UserData;
use App\Form\UserEditData;
use App\Form\UserEditForm;
use App\Service\RegistrationTokenStorageInterface;
use App\Service\TokenSerializerInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * Leasing calculator controller that shows the leasing calculation form
 */
class LeasingCalculatorController extends Controller
{
    const SESSION_KEY_LEASING_TAKER_STEP = 'leasingCalculatorForm::leasingTakerProfileStep';
    const SESSION_KEY_CREDITORS_STEP = 'leasingCalculatorForm::creditorsStep';
    const SESSION_KEY_LEASING_DETAILS_STEP = 'leasingCalculatorForm::leasingDetailsStep';

    /**
     * @Route("/leasing-calculator", name="leasing-calculator")
     * @Method({"GET","POST"})
     * @param Request $request
     * @param Session $session
     * @return Response
     */
    public function leasingTakerProfileStep(Request $request, Session $session)
    {
        $session->start();
        $currentFormData = $session->get(static::SESSION_KEY_LEASING_TAKER_STEP);
        $data = new LeasingTakerProfileData();
        if(null !== $currentFormData && strlen($currentFormData) > 0) {
            $data->deserialize($currentFormData);
        }
        $form = $this->createForm(LeasingTakerProfileForm::class, $data);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if (!$form->isValid()) {
                $response = $this->render('leasingCalculatorLeasingTakerProfileForm.html.twig', ['form' => $form->createView()]);
                $response->setStatusCode(400);
                return $response;
            }

            $session->set(static::SESSION_KEY_LEASING_TAKER_STEP, $data->serialize());

            return $this->redirectToRoute('leasing-calculator-creditors');

        }
        return $this->render('leasingCalculatorLeasingTakerProfileForm.html.twig', ['form' => $form->createView()]);
    }


    /**
     * @Route("/leasing-calculator-creditors", name="leasing-calculator-creditors")
     * @Method({"GET","POST"})
     * @param Request $request
     * @param Session $session
     * @return Response
     */
    public function multipleCreditorsStep(Request $request, Session $session)
    {
        $session->start();
        $currentFormData = $session->get(static::SESSION_KEY_CREDITORS_STEP);
        $data = new MultipleCreditorProfilesData();
        if(null !== $currentFormData && strlen($currentFormData) > 0) {
            $data->deserialize($currentFormData);
        } else {
            $data->creditors->add(new CreditorProfileData());
        }
        $creditorsStartIndex = $data->creditors->count();
        $form = $this->createForm(MultipleCreditorProfilesForm::class, $data);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if (!$form->isValid()) {
                $response = $this->render('leasingCalculatorCreditorsProfilesForm.html.twig',
                    ['form' => $form->createView(), 'creditorsStartIndex' => $creditorsStartIndex]);
                $response->setStatusCode(400);
                return $response;
            }

            $session->set(static::SESSION_KEY_CREDITORS_STEP, $data->serialize());
            return $this->redirectToRoute('leasing-calculator-details');

        }
        return $this->render('leasingCalculatorCreditorsProfilesForm.html.twig',
            ['form' => $form->createView(), 'creditorsStartIndex' => $creditorsStartIndex]);
    }
    /**
     * @Route("/leasing-calculator-details", name="leasing-calculator-details")
     * @Method({"GET","POST"})
     * @param Request $request
     * @param Session $session
     * @return Response
     */
    public function leasingDetailsStep(Request $request, Session $session)
    {
        $session->start();
        $currentFormData = $session->get(static::SESSION_KEY_LEASING_DETAILS_STEP);
        $data = new LeasingDetailsData();
        if(null !== $currentFormData && strlen($currentFormData) > 0) {
            $data->deserialize($currentFormData);
        }

        $form = $this->createForm(LeasingDetailsForm::class, $data);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if (!$form->isValid()) {
                $response = $this->render('leasingCalculatorLeasingDetailsForm.html.twig',
                    ['form' => $form->createView()]);
                $response->setStatusCode(400);
                return $response;
            }

            $session->set(static::SESSION_KEY_LEASING_DETAILS_STEP, $data->serialize());
            return $this->redirectToRoute('leasing-calculator-result');

        }
        return $this->render('leasingCalculatorLeasingDetailsForm.html.twig',
            ['form' => $form->createView()]);
    }

    /**
     * @Route("/leasing-calculator-result", name="leasing-calculator-result")
     * @Method({"GET","POST"})
     * @param Request $request
     * @param Session $session
     * @return Response
     */
    public function leasingResultStep(Request $request, Session $session)
    {
        $takerProfile = json_decode($session->get(static::SESSION_KEY_LEASING_TAKER_STEP), true);
        $creditorsData = json_decode($session->get(static::SESSION_KEY_CREDITORS_STEP), true);
        $leasingDetails = json_decode($session->get(static::SESSION_KEY_LEASING_DETAILS_STEP), true);
        return $this->json(['takerProfile'=>$takerProfile, 'creditorsData'=>$creditorsData, 'leasingDetails' => $leasingDetails]);
    }

    /**
     * @Route("/leasing-calculator-clear", name="leasing-calculator-clear")
     * @Method({"GET","POST"})
     * @param Request $request
     * @param Session $session
     * @return Response
     */
    public function leasingClearData(Request $request, Session $session)
    {
        $session->set(static::SESSION_KEY_LEASING_TAKER_STEP, null);
        $session->set(static::SESSION_KEY_CREDITORS_STEP, null);
        $session->set(static::SESSION_KEY_LEASING_DETAILS_STEP, null);
        return $this->redirectToRoute('leasing-calculator');
    }
}
