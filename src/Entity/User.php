<?php
declare(strict_types=1);

namespace App\Entity;

use App\Form\UserEditData;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

class User implements UserInterface, EquatableInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $loginToken;
    /**
     * @var string
     */
    private $businessName;
    /**
     * @var string
     */
    private $vinNumber;
    /**
     * @var string
     */
    private $email;
    /**
     * @var ?int
     */
    private $foundationYear;
    /**
     * @var float
     */
    private $annualIncome;
    /**
     * @var bool
     */
    private $allowShareToNSAAndTaxOffice;
    /**
     * @var bool
     */
    private $isWashingMoney;
    /**
     * @var string
     */
    private $registrationToken;

    /**
     * User constructor.
     * @param string $loginToken
     * @param string $businessName
     * @param string $vinNumber
     * @param string $email
     * @param int $foundationYear
     * @param float $annualIncome
     * @param bool $allowShareToNSAAndTaxOffice
     * @param bool $isWashingMoney
     * @param string $registrationToken
     */
    public function __construct(string $loginToken, string $businessName, string $vinNumber, string $email, ?int $foundationYear, float $annualIncome,
                                bool $allowShareToNSAAndTaxOffice, bool $isWashingMoney, string $registrationToken)
    {
        $this->loginToken = $loginToken;
        $this->businessName = $businessName;
        $this->vinNumber = $vinNumber;
        $this->email = $email;
        $this->foundationYear = $foundationYear;
        $this->annualIncome = $annualIncome;
        $this->allowShareToNSAAndTaxOffice = $allowShareToNSAAndTaxOffice;
        $this->isWashingMoney = $isWashingMoney;
        $this->registrationToken = $registrationToken;
    }


    /**
     * @return string
     */
    public function getBusinessName(): string
    {
        return $this->businessName;
    }

    /**
     * @param string $businessName
     */
    public function setBusinessName(string $businessName): void
    {
        $this->businessName = $businessName;
    }

    /**
     * @return string
     */
    public function getVinNumber(): string
    {
        return $this->vinNumber;
    }

    /**
     * @param string $vinNumber
     */
    public function setVinNumber(string $vinNumber): void
    {
        $this->vinNumber = $vinNumber;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getFoundationYear(): ?int
    {
        return $this->foundationYear;
    }

    /**
     * @param int $foundationYear
     */
    public function setFoundationYear(?int $foundationYear): void
    {
        $this->foundationYear = $foundationYear;
    }

    /**
     * @return double
     */
    public function getAnnualIncome(): float
    {
        return $this->annualIncome;
    }

    /**
     * @return string
     */
    public function getLoginToken(): string
    {
        return $this->loginToken;
    }

    /**
     * @param string $loginToken
     */
    public function setLoginToken(string $loginToken): void
    {
        $this->loginToken = $loginToken;
    }

    /**
     * @param double $annualIncome
     */
    public function setAnnualIncome(float $annualIncome): void
    {
        $this->annualIncome = $annualIncome;
    }

    /**
     * @return bool
     */
    public function isAllowShareToNSAAndTaxOffice(): bool
    {
        return $this->allowShareToNSAAndTaxOffice;
    }

    /**
     * @param bool $allowShareToNSAAndTaxOffice
     */
    public function setAllowShareToNSAAndTaxOffice(bool $allowShareToNSAAndTaxOffice): void
    {
        $this->allowShareToNSAAndTaxOffice = $allowShareToNSAAndTaxOffice;
    }

    /**
     * @return bool
     */
    public function isWashingMoney(): bool
    {
        return $this->isWashingMoney;
    }

    /**
     * @param bool $isWashingMoney
     */
    public function setIsWashingMoney(bool $isWashingMoney): void
    {
        $this->isWashingMoney = $isWashingMoney;
    }

    /**
     * @return string
     */
    public function getRegistrationToken(): string
    {
        return $this->registrationToken;
    }

    /**
     * @param string $registrationToken
     */
    public function setRegistrationToken(string $registrationToken): void
    {
        $this->registrationToken = $registrationToken;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->loginToken;
    }

    /**
     * @return string
     */
    public function getSalt(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->loginToken;
    }

    /**
     */
    public function eraseCredentials(): void
    {
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    public function isEqualTo(UserInterface $user): bool
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->loginToken !== $user->getPassword()) {
            return false;
        }

        return true;
    }

    public function loadFromUserEditData(UserEditData $userData): void
    {
        $this->setEmail($userData->email);
        $this->setAnnualIncome($userData->annualIncome);
        $this->setIsWashingMoney($userData->washMoney);
    }

}
