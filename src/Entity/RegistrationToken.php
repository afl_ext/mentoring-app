<?php
declare(strict_types=1);

namespace App\Entity;

class RegistrationToken
{
    /**
     * @var int
     */
    private $id;

    /**
     * Token not-serialized text representation
     * @var string
     */
    private $token;

    /**
     * @var boolean
     */
    private $active;

    /**
     * Unix timestamp representing creation time of the token
     * @var integer
     */
    private $createTime;

    /**
     * @param string $tokenString textual, not serialized representation of token
     * @param boolean $isActive
     * @param integer $creationTime unix timestamp representing creation time
     */
    public function __construct(string $tokenString, bool $isActive, int $creationTime)
    {
        $this->token = $tokenString;
        $this->active = $isActive;
        $this->createTime = $creationTime;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return boolean
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    public function activate(): void
    {
        $this->active = true;
    }

    public function deactivate(): void
    {
        $this->active = false;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->getActive();
    }

    /**
     * @return integer
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * @param integer $createTime
     */
    public function setCreateTime($createTime): void
    {
        $this->createTime = $createTime;
    }

}
