<?php
declare(strict_types=1);

namespace App\Command;

use App\Exception\RegistrationTokenLimitExceededException;
use App\Service\RegistrationTokenGeneratorInterface;
use App\Service\TokenSerializerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Command that makes it possible to generate new registration tokens
 * no parameters are needed
 */
class CreateRegistrationTokenCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:generate:registration-token';

    /**
     * @var RegistrationTokenGeneratorInterface
     */
    private $registrationTokenGenerator;

    /**
     * @var TokenSerializerInterface
     */
    private $tokenSerializer;

    /**
     * @param RegistrationTokenGeneratorInterface $registrationTokenGenerator
     * @param TokenSerializerInterface $tokenSerializer
     */
    public function __construct(RegistrationTokenGeneratorInterface $registrationTokenGenerator, TokenSerializerInterface $tokenSerializer)
    {
        parent::__construct();
        $this->registrationTokenGenerator = $registrationTokenGenerator;
        $this->tokenSerializer = $tokenSerializer;
    }

    /**
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Generates new registration token');
    }

    /**
     * Function that is a main entry to the command, generates a token, and outputs it if it succeeds, or display an error if it fails
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $newToken = $this->registrationTokenGenerator->generateNewToken();
            $serialized = $this->tokenSerializer->transform($newToken);
            $io->success(sprintf("Generated new registration token:\nRaw format: %s\nSerialized: %s", $newToken, $serialized));
        } catch (RegistrationTokenLimitExceededException $ex){
            $io->error($ex->getMessage());
        }
    }
}
