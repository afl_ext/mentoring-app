<?php

namespace App\Form;


use App\Entity\User;

class UserEditData
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var float
     */
    public $annualIncome;
    /**
     * @var bool
     */
    public $washMoney;

    /**
     * @param User $user
     */
    public function loadFromUser(User $user): void
    {
        $this->email = $user->getEmail();
        $this->annualIncome = $user->getAnnualIncome();
        $this->washMoney = $user->isWashingMoney();
    }
}