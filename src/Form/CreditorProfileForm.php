<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Service\TokenSerializerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Translation\TranslatorInterface;


/**
 * Single creditor profiles forms
 */
class CreditorProfileForm extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $currentYear = intval(date('Y'));
        $balanceInText = $this->translator->trans('CORE.BALANCE_IN');
        $last5YearsLabels = [
            $balanceInText.' '.strval($currentYear - 1),
            $balanceInText.' '.strval($currentYear - 2),
            $balanceInText.' '.strval($currentYear - 3),
            $balanceInText.' '.strval($currentYear - 4),
            $balanceInText.' '.strval($currentYear - 5)
        ];
        $builder
            ->add('birthDate', BirthdayType::class, ['label' => 'CORE.BIRTH_DATE', 'empty_data' => ''])
            ->add('nipNumber', TextType::class, ['label' => 'CORE.NIP_NUMBER', 'empty_data' => ''])
            ->add('regonNumber', TextType::class, ['label' => 'CORE.REGON_NUMBER', 'empty_data' => ''])
            ->add('peselNumber', TextType::class, ['label' => 'CORE.PESEL_NUMBER', 'empty_data' => ''])
            ->add('businessFoundationDate', DateType::class, ['years' => range(1201,2018), 'label' => 'CORE.BUSINESS_FOUNDATION_DATE', 'empty_data' => ''])
            ->add('balance1', NumberType::class, ['label' => $last5YearsLabels[0]])
            ->add('balance2', NumberType::class, ['label' => $last5YearsLabels[1]])
            ->add('balance3', NumberType::class, ['label' => $last5YearsLabels[2]])
            ->add('balance4', NumberType::class, ['label' => $last5YearsLabels[3]])
            ->add('balance5', NumberType::class, ['label' => $last5YearsLabels[4]])
            ->add('isACreditor', CheckboxType::class, ['label' => 'CORE.IS_A_CREDITOR', 'required' => false]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CreditorProfileData::class,
            'allow_extra_fields' => true
        ));
    }
}