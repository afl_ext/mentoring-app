<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Service\TokenSerializerInterface;
use App\Service\WIBORProviderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Translation\TranslatorInterface;


/**
 * Single creditor profiles forms
 */
class LeasingDetailsForm extends AbstractType
{

    private $WIBORProvider;

    public function __construct(WIBORProviderInterface $WIBORProvider)
    {
        $this->WIBORProvider = $WIBORProvider;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $currencies = [
            'PLN' => 'PLN',
            'EUR' => 'EUR',
            'USD' => 'USD',
            'CAD' => 'CAD',
            'CHF' => 'CHF',
            'UAH' => 'UAH'];
        $WIBOR = $this->WIBORProvider->getWIBOR();
        $builder
            ->add('itemValue', NumberType::class, ['label' => 'CORE.ITEM_VALUE', 'empty_data' => ''])
            ->add('installmentCount', NumberType::class, ['label' => 'CORE.INSTALLMENT_COUNT', 'empty_data' => ''])
            ->add('initialPaymentPercent', NumberType::class, ['label' => 'CORE.INSTALLMENT_PAYMENT_PERCENT', 'empty_data' => ''])
            ->add('repurchasePaymentPercent', NumberType::class, ['label' => 'CORE.REPURCHASE_PAYMENT_PERCENT', 'empty_data' => ''])
            ->add('leasingGiverMargin', NumberType::class, ['label' => 'CORE.LEASING_GIVER_MARGIN', 'empty_data' => ''])
            ->add('WIBOR', NumberType::class, ['attr' => ['readonly' => true], 'data' => $WIBOR, 'mapped' => false, 'label' => 'CORE.WIBOR', 'empty_data' => ''])
            ->add('currency', CurrencyType::class, ['label' => 'CORE.CURRENCY', 'empty_data' => '',
            'choice_loader' => null, 'choices' => $currencies]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {

    }
}