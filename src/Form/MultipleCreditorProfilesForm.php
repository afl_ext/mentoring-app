<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Service\TokenSerializerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\Valid;


/**
 * Multiple creditor profiles forms
 */
class MultipleCreditorProfilesForm extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('creditors',  CollectionType::class, [
                'entry_type' => CreditorProfileForm::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'label' => 'CORE.CREDITORS',
                'constraints' => array(new Valid()),
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'cascade_validation' => true
        ]);
    }
}