<?php

namespace App\Form;


class CreditorProfileData
{

    /**
     * @var \DateTime
     */
    public $birthDate;
    /**
     * @var int
     */
    public $nipNumber;
    /**
     * @var int
     */
    public $regonNumber;
    /**
     * @var int
     */
    public $peselNumber;
    /**
     * @var \DateTime
     */
    public $businessFoundationDate;
    /**
     * @var float
     */
    public $balance1;
    /**
     * @var float
     */
    public $balance2;
    /**
     * @var float
     */
    public $balance3;
    /**
     * @var float
     */
    public $balance4;
    /**
     * @var float
     */
    public $balance5;
    /**
     * @var bool
     */
    public $isACreditor;

    public function getDataArray(): array
    {
        return [
            'birthDate' => $this->birthDate->getTimestamp(),
            'nipNumber' => $this->nipNumber,
            'regonNumber' => $this->regonNumber,
            'peselNumber' => $this->peselNumber,
            'businessFoundationDate' => $this->businessFoundationDate->getTimestamp(),
            'balance1' => $this->balance1,
            'balance2' => $this->balance2,
            'balance3' => $this->balance3,
            'balance4' => $this->balance4,
            'balance5' => $this->balance5,
            'isACreditor' => $this->isACreditor
        ];
    }

    public function setDataFromArray($data): void
    {
        $this->birthDate = (new \DateTime())->setTimestamp($data['birthDate']);
        $this->nipNumber = $data['nipNumber'];
        $this->regonNumber = $data['regonNumber'];
        $this->peselNumber = $data['peselNumber'];
        $this->businessFoundationDate = (new \DateTime())->setTimestamp($data['businessFoundationDate']);
        $this->balance1 = $data['balance1'];
        $this->balance2 = $data['balance2'];
        $this->balance3 = $data['balance3'];
        $this->balance4 = $data['balance4'];
        $this->balance5 = $data['balance5'];
        $this->isACreditor = $data['isACreditor'];
    }
}