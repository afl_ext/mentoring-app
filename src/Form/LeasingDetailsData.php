<?php

namespace App\Form;


class LeasingDetailsData
{

    /**
     * @var float
     */
    public $itemValue;

    /**
     * @var int
     */
    public $installmentCount;

    /**
     * @var float
     */
    public $initialPaymentPercent;

    /**
     * @var float
     */
    public $repurchasePaymentPercent;

    /**
     * @var float
     */
    public $leasingGiverMargin;

    /**
     * @var float
     */
    public $WIBOR;

    /**
     * @var string
     */
    public $currency;

    public function serialize(): string
    {
        return json_encode([
            'itemValue' => $this->itemValue,
            'installmentCount' => $this->installmentCount,
            'initialPaymentPercent' => $this->initialPaymentPercent,
            'repurchasePaymentPercent' => $this->repurchasePaymentPercent,
            'leasingGiverMargin' => $this->leasingGiverMargin,
            'WIBOR' => $this->WIBOR,
            'currency' => $this->currency
        ]);
    }

    public function deserialize($serialized): void
    {
        $data = json_decode($serialized, true);
        $this->itemValue = $data['itemValue'];
        $this->installmentCount = $data['installmentCount'];
        $this->initialPaymentPercent = $data['initialPaymentPercent'];
        $this->repurchasePaymentPercent = $data['repurchasePaymentPercent'];
        $this->leasingGiverMargin = $data['leasingGiverMargin'];
        $this->WIBOR = $data['WIBOR'];
        $this->currency = $data['currency'];
    }
}