<?php

namespace App\Form;


class LeasingTakerProfileData
{
    /**
     * @var \DateTime
     */
    public $birthDate;
    /**
     * @var int
     */
    public $nipNumber;
    /**
     * @var int
     */
    public $regonNumber;
    /**
     * @var int
     */
    public $peselNumber;
    /**
     * @var \DateTime
     */
    public $businessFoundationDate;
    /**
     * @var float
     */
    public $balance1;
    /**
     * @var float
     */
    public $balance2;
    /**
     * @var float
     */
    public $balance3;
    /**
     * @var float
     */
    public $balance4;
    /**
     * @var float
     */
    public $balance5;
    /**
     * @var bool
     */
    public $isInDebtCheckbox;
    /**
     * @var float
     */
    public $debtAmount;
    /**
     * @var bool
     */
    public $isInKRD;

    public function serialize(): string
    {
        return json_encode([
            'birthDate' => $this->birthDate->getTimestamp(),
            'nipNumber' => $this->nipNumber,
            'regonNumber' => $this->regonNumber,
            'peselNumber' => $this->peselNumber,
            'businessFoundationDate' => $this->businessFoundationDate->getTimestamp(),
            'balance1' => $this->balance1,
            'balance2' => $this->balance2,
            'balance3' => $this->balance3,
            'balance4' => $this->balance4,
            'balance5' => $this->balance5,
            'isInDebtCheckbox' => $this->isInDebtCheckbox,
            'debtAmount' => $this->debtAmount,
            'isInKRD' => $this->isInKRD
        ]);
    }

    public function deserialize($serialized): void
    {
        $data = json_decode($serialized, true);
        $this->birthDate = (new \DateTime())->setTimestamp($data['birthDate']);
        $this->nipNumber = $data['nipNumber'];
        $this->regonNumber = $data['regonNumber'];
        $this->peselNumber = $data['peselNumber'];
        $this->businessFoundationDate = (new \DateTime())->setTimestamp($data['businessFoundationDate']);
        $this->balance1 = $data['balance1'];
        $this->balance2 = $data['balance2'];
        $this->balance3 = $data['balance3'];
        $this->balance4 = $data['balance4'];
        $this->balance5 = $data['balance5'];
        $this->isInDebtCheckbox = $data['isInDebtCheckbox'];
        $this->debtAmount = $data['debtAmount'];
        $this->isInKRD = $data['isInKRD'];
    }
}