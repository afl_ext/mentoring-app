<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Service\TokenSerializerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


/**
 * Form that user uses to register new user account using registration token and business data
 */
class RegistrationForm extends AbstractType
{
    private $tokenSerializer;

    public function __construct(TokenSerializerInterface $tokenSerializer)
    {
        $this->tokenSerializer = $tokenSerializer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('businessName', TextType::class, ['attr' => ['class'=>'form-control'], 'label' => 'CORE.BUSINESS_NAME', 'empty_data'=>''])
            ->add('vinNumber', NumberType::class, ['attr' => ['class'=>'form-control'], 'label' => 'CORE.VIN_NUMBER', 'empty_data'=>''])
            ->add('email', EmailType::class, ['attr' => ['class'=>'form-control'], 'label' => 'CORE.EMAIL', 'empty_data'=>''])
            ->add('foundationYear', NumberType::class, ['required' => false, 'attr' => ['class'=>'form-control'], 'label' => 'CORE.FOUNDATION_YEAR', 'empty_data'=>''])
            ->add('annualIncome', NumberType::class, ['attr' => ['class'=>'form-control'], 'label' => 'CORE.Annual_INCOME', 'empty_data'=>''])
            ->add('washMoney', CheckboxType::class, ['required' => false, 'attr' => ['class'=>'form-check-input'], 'label' => 'CORE.WASH_MONEY_QUESTION'])
            ->add('shareNSATax', CheckboxType::class, ['attr' => ['class'=>'form-check-input'], 'label' => 'CORE.SHARE_DATA_NSA_TAX_QUESTION', 'empty_data'=>''])
            ->add('registrationToken', TextType::class, ['attr' => ['class'=>'form-control'], 'label' => 'CORE.REGISTRATION_TOKEN', 'empty_data'=>'']);

        $builder->get('registrationToken')
            ->addModelTransformer($this->tokenSerializer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {

    }
}