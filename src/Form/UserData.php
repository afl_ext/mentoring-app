<?php

namespace App\Form;


class UserData
{
    /**
     * @var string
     */
    public $businessName;
    /**
     * @var string
     */
    public $vinNumber;
    /**
     * @var string
     */
    public $email;
    /**
     * @var ?int
     */
    public $foundationYear;
    /**
     * @var float
     */
    public $annualIncome;
    /**
     * @var bool
     */
    public $shareNSATax;
    /**
     * @var bool
     */
    public $washMoney;
    /**
     * @var string
     */
    public $registrationToken;
}