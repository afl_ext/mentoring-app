<?php

namespace App\Form;


use Doctrine\Common\Collections\ArrayCollection;

class MultipleCreditorProfilesData
{
    public $creditors;

    public function __construct()
    {
        $this->creditors = new ArrayCollection();
    }

    public function serialize(): string
    {
        $creditorsArray = $this->creditors->getValues();
        $creditorsDataArray = [];
        foreach ($creditorsArray as $creditor) {
            $creditorsDataArray[] = $creditor->getDataArray();
        }
        return json_encode($creditorsDataArray);
    }

    public function deserialize($serialized): void
    {
        $creditorsDataArray = json_decode($serialized, true);
        $this->creditors->clear();
        foreach ($creditorsDataArray as $dataElement) {
            $creditor = new CreditorProfileData();
            $creditor->setDataFromArray($dataElement);
            $this->creditors->add($creditor);
        }
    }

}