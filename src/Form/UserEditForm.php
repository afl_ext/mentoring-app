<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Service\TokenSerializerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


/**
 * Form that allows the user to edit its own data
 */
class UserEditForm extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['attr' => ['class' => 'form-control'], 'label' => 'CORE.EMAIL', 'empty_data' => ''])
            ->add('annualIncome', NumberType::class, ['attr' => ['class' => 'form-control'], 'label' => 'CORE.Annual_INCOME', 'empty_data' => ''])
            ->add('washMoney', CheckboxType::class, ['required' => false, 'label' => 'CORE.WASH_MONEY_QUESTION']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {

    }
}