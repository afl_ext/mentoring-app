<?php
declare(strict_types=1);

namespace App\Service;


use App\Entity\RegistrationToken;
use App\Repository\RegistrationTokenRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Implements token storage interface using database as token storage
 */
class DatabaseRegistrationTokenStorage implements RegistrationTokenStorageInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RegistrationTokenRepository
     */
    private $registrationTokenRepository;
    /**
     * @var TimeProviderInterface
     */
    private $timeProvider;

    /**
     * @param EntityManagerInterface $entityManager
     * @param RegistrationTokenRepository $registrationTokenRepository
     * @param TimeProviderInterface $timeProvider
     */
    public function __construct(EntityManagerInterface $entityManager, RegistrationTokenRepository $registrationTokenRepository,
                                TimeProviderInterface $timeProvider)
    {
        $this->entityManager = $entityManager;
        $this->registrationTokenRepository = $registrationTokenRepository;
        $this->timeProvider = $timeProvider;
    }

    /**
     * @param string $token
     */
    public function saveRegistrationToken(string $token): void
    {
        $token = new RegistrationToken($token, true, $this->timeProvider->getUnixTimestamp());
        $this->entityManager->persist($token);
        $this->entityManager->flush();
    }

    /**
     * @param string $token
     * @return bool
     */
    public function isActiveToken(string $token): bool
    {
        $tokenSearchResult = $this->registrationTokenRepository->findByToken($token);
        if(null === $tokenSearchResult){
            return false;
        }
        return $tokenSearchResult->isActive();
    }

    /**
     * @param string $token
     */
    public function deactivateToken(string $token): void
    {
        $tokenSearchResult = $this->registrationTokenRepository->findByToken($token);
        if(null !== $tokenSearchResult){
            $tokenSearchResult->deactivate();
            $this->entityManager->persist($tokenSearchResult);
            $this->entityManager->flush();
        }
    }
}