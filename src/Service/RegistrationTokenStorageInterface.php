<?php
declare(strict_types=1);

namespace App\Service;


/**
 * Interface for storing registration tokens. Allow to store tokens in different ways using common interface
 */
interface RegistrationTokenStorageInterface
{
    /**
     * @param string $token
     */
    public function saveRegistrationToken(string $token): void;

    /**
     * @param string $token
     * @return bool
     */
    public function isActiveToken(string $token): bool;

    /**
     * @param string $token
     */
    public function deactivateToken(string $token): void;
}