<?php
declare(strict_types=1);
namespace App\Service;


use App\Entity\RegistrationToken;
use App\Exception\RegistrationTokenLimitExceededException;

/**
 *
 */
class RegistrationTokenLimitChecker implements RegistrationTokenLimitCheckerInterface
{
    /**
     * @var RegistrationTokenLimitConfigurationProviderInterface
     */
    private $limitConfigurationProvider;
    /**
     * @var TimeProviderInterface
     */
    private $timeProvider;
    /**
     * @var int
     */
    private $tokenCountLimit;
    /**
     * @var int
     */
    private $tokenTimeoutSec;

    /**
     * @var RegistrationTokenCountPerDayProviderInterface
     */
    private $registrationTokenCountPerDayProvider;

    /**
     * RegistrationTokenLimitChecker constructor.
     * @param RegistrationTokenLimitConfigurationProviderInterface $limitConfigurationProvider
     * @param TimeProviderInterface $timeProvider
     * @param RegistrationTokenCountPerDayProviderInterface $registrationTokenCountPerDayProvider
     * @param int $tokenCountLimit
     * @param int $tokenTimeoutSec
     */
    public function __construct(RegistrationTokenLimitConfigurationProviderInterface $limitConfigurationProvider,
                                TimeProviderInterface $timeProvider, RegistrationTokenCountPerDayProviderInterface $registrationTokenCountPerDayProvider,
                                int $tokenCountLimit, int $tokenTimeoutSec)
    {
        $this->limitConfigurationProvider = $limitConfigurationProvider;
        $this->tokenCountLimit = $tokenCountLimit;
        $this->tokenTimeoutSec = $tokenTimeoutSec;
        $this->timeProvider = $timeProvider;
        $this->registrationTokenCountPerDayProvider = $registrationTokenCountPerDayProvider;
    }

    /**
     * @throws RegistrationTokenLimitExceededException
     */
    public function throwIfNewTokenNotAllowed(): void
    {
        $counter = $this->limitConfigurationProvider->getCounterValue();
        $timestampAllowAgain = $this->limitConfigurationProvider->getAllowAfterTime();
        if($counter < $this->tokenCountLimit && $timestampAllowAgain < $this->timeProvider->getUnixTimestamp()) {
            return;
        }
        if($timestampAllowAgain < $this->timeProvider->getUnixTimestamp()){
            $this->resetTimeCounter();
        } else {
            $nextTryFormattedTime = date('Y-m-d H:i', $timestampAllowAgain);
            throw new RegistrationTokenLimitExceededException(
                sprintf('Registration token generation limits exceeded, try again after: %s', $nextTryFormattedTime));
        }
    }

    private function resetTimeCounter(): void
    {
        $this->limitConfigurationProvider->resetCounterValue();
    }

    public function registerNewToken(): void
    {
        $this->limitConfigurationProvider->incrementCounterValue();
        $this->registrationTokenCountPerDayProvider->registerNewToken();
        $counter = $this->limitConfigurationProvider->getCounterValue();
        if($counter >= $this->tokenCountLimit ){
            $this->limitConfigurationProvider->setAllowAfterTime($this->timeProvider->getUnixTimestamp() + $this->tokenTimeoutSec);
        }
    }
}