<?php
declare(strict_types=1);
namespace App\Service;


/**
 * Interface for generating registration tokens string not serialized representation
 */
interface RegistrationTokenStringGeneratorInterface
{
    /**
     * Generate token string
     * @return string
     */
    public function generate(): string;
}