<?php
declare(strict_types=1);

namespace App\Service;

/**
 * Interface for storing key-value data
 */
interface KeyValueStoreInterface
{
    /**
     * @param string $key
     * @return null|string
     */
    public function getValueByKey(string $key): ?string;

    /**
     * @param string $key
     * @param string $value
     */
    public function setValueByKey(string $key, string $value): void;
}