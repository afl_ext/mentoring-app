<?php
declare(strict_types=1);
namespace App\Service;


/**
 * Class implementing TokenSerializerInterface to serialize tokens using not secure base64 encoding
 */
class Base64TokenSerializer implements TokenSerializerInterface
{

    /**
     * Serialize a token to base64
     * @param string $rawRepresentation
     * @return string
     */
    public function transform($rawRepresentation): ?string
    {
        if(null === $rawRepresentation) {
            return null;
        }
        return base64_encode($rawRepresentation);
    }

    /**
     * Deserialize a token from base64
     * @param string $serializedRepresentation
     * @return string
     */
    public function reverseTransform($serializedRepresentation): string
    {
        if(null === $serializedRepresentation) {
            return null;
        }
        try {
            $string = base64_decode($serializedRepresentation);
            if(preg_match('/[^\x1F-\x7E\xE2\x80\x99]/', $string)) {
                return "";
            }
            else {
                return $string;
            }
        } catch (\Exception $e){
            return "";
        }
    }
}