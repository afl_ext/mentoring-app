<?php
declare(strict_types=1);

namespace App\Service;


/**
 * Implements RegistrationTokenLimitConfigurationProviderInterface using a key-value store
 */
class RegistrationTokenLimitConfigurationProvider implements RegistrationTokenLimitConfigurationProviderInterface
{
    /**
     * @var KeyValueStoreInterface
     */
    private $keyValueStore;
    /**
     * @const string
     */
    private const TOKEN_GENERATION_COUNTER_KEY = 'token_generation_counter';
    /**
     * @const string
     */
    private const TOKEN_GENERATION_ALLOW_AFTER_KEY = 'token_allow_after';

    /**
     * @param KeyValueStoreInterface $keyValueStore
     */
    public function __construct(KeyValueStoreInterface $keyValueStore)
    {
        $this->keyValueStore = $keyValueStore;
        $this->setDefaultKeyStoreValue(static::TOKEN_GENERATION_COUNTER_KEY, '0');
        $this->setDefaultKeyStoreValue(static::TOKEN_GENERATION_ALLOW_AFTER_KEY, '0');
    }

    /**
     * @param string $key
     * @param string $value
     */
    private function setDefaultKeyStoreValue(string $key, string $value): void
    {
        $storeValue = $this->keyValueStore->getValueByKey($key);
        if(null == $storeValue) {
            $this->keyValueStore->setValueByKey($key, $value);
        }
    }

    /**
     * @param string $key
     * @return int
     */
    private function extractInteger(string $key): int
    {
        return (int)$this->keyValueStore->getValueByKey($key);
    }

    /**
     * @param string $key
     * @param int $value
     */
    private function saveInteger(string $key, int $value): void
    {
        $this->keyValueStore->setValueByKey($key, strval($value));
    }

    /**
     * @return int
     */
    public function getCounterValue(): int
    {
        return $this->extractInteger(static::TOKEN_GENERATION_COUNTER_KEY);
    }

    /**
     *
     */
    public function incrementCounterValue(): void
    {
        $counter = $this->extractInteger(static::TOKEN_GENERATION_COUNTER_KEY);
        $this->saveInteger(static::TOKEN_GENERATION_COUNTER_KEY, $counter + 1);
    }

    public function resetCounterValue(): void
    {
        $this->saveInteger(static::TOKEN_GENERATION_COUNTER_KEY, 0);
    }

    /**
     * Gets the time after which token generation will be re-enabled
     * @return int
     */
    public function getAllowAfterTime(): int
    {
        return $this->extractInteger(static::TOKEN_GENERATION_ALLOW_AFTER_KEY);
    }

    /**
     * Sets the time when token generation will be re-enabled
     * @param int $timestamp
     */
    public function setAllowAfterTime(int $timestamp): void
    {
        $this->saveInteger(static::TOKEN_GENERATION_ALLOW_AFTER_KEY, $timestamp);
    }
}