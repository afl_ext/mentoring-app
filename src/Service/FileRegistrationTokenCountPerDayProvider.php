<?php
declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class FileRegistrationTokenCountPerDayProvider implements RegistrationTokenCountPerDayProviderInterface
{

    /**
     * @var TimeProviderInterface
     */
    private $timeProvider;
    /**
     * @var string
     */
    private $storagePath;
    /**
     * @var Filesystem
     */
    private $fileSystem;


    /**
     * @param TimeProviderInterface $timeProvider
     * @param Filesystem $fileSystem
     * @param string $storagePath
     */
    public function __construct(TimeProviderInterface $timeProvider, Filesystem $fileSystem, string $storagePath)
    {
        $this->timeProvider = $timeProvider;
        $this->storagePath = $storagePath;
        $this->fileSystem = $fileSystem;
    }

    /**
     * @return array
     */
    private function readAndDecodeCounterFile(): array
    {
        if(!$this->fileSystem->exists($this->storagePath)){
            $this->setCounterValue(0);
        }
        $fileInfo = new File($this->storagePath);
        $file = $fileInfo->openFile('r');
        $contents = $file->fread($file->getSize());
        $data = json_decode($contents, true);
        return $data;
    }

    /**
     * @return int
     */
    private function getCounterValue(): int
    {
        $data = $this->readAndDecodeCounterFile();
        return $data['value'];
    }

    /**
     * @return string
     */
    private function getCounterDateString(): string
    {
        $data = $this->readAndDecodeCounterFile();
        return $data['dateString'];
    }

    /**
     * @return bool
     */
    private function shouldResetCounter(): bool
    {
        $currentDateString = $this->timeProvider->getFormattedDateTimeString('Y-m-d');
        $savedDateString = $this->getCounterDateString();
        return $currentDateString !== $savedDateString;
    }

    /**
     * @param int $value
     */
    private function setCounterValue(int $value): void
    {
        $dateString = $this->timeProvider->getFormattedDateTimeString('Y-m-d');
        $jsonData = json_encode(['dateString' => $dateString, 'value' => $value]);
        $this->fileSystem->dumpFile($this->storagePath, $jsonData);
    }

    /**
     * Increments a counter in file to provide tokens generated per day
     */
    public function registerNewToken(): void
    {
        if ($this->shouldResetCounter()) {
            $this->setCounterValue(0);
        } else {
            $currentCount = $this->getCounterValue();
            $this->setCounterValue($currentCount + 1);
        }
    }

    /**
     * @return int
     */
    public function getTokenCountGeneratedToday(): int
    {
        if ($this->shouldResetCounter()) {
            $this->setCounterValue(0);
            return 0;
        } else {
            return $this->getCounterValue();

        }
    }
}