<?php
declare(strict_types=1);

namespace App\Service;


/**
 * Provides real time
 */
class CurrentTimeProvider implements TimeProviderInterface
{

    /**
     * @return int
     */
    public function getUnixTimestamp(): int
    {
        return (new \DateTime())->getTimestamp();
    }

    /**
     * @param string $format
     * @return string
     */
    public function getFormattedDateTimeString(string $format): string
    {
        return (new \DateTime())->format($format);
    }
}