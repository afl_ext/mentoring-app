<?php
declare(strict_types=1);
namespace App\Service;


use App\Entity\RegistrationToken;
use App\Exception\RegistrationTokenLimitExceededException;

/**
 * Interface for checking registration token limits
 */
interface RegistrationTokenLimitCheckerInterface
{
    /**
     * @throws RegistrationTokenLimitExceededException
     */
    public function throwIfNewTokenNotAllowed(): void;

    public function registerNewToken(): void;
}