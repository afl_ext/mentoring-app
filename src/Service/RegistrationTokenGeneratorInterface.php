<?php
declare(strict_types=1);
namespace App\Service;


/**
 * Interface for generating new registration tokens
 */
interface RegistrationTokenGeneratorInterface
{
    /**
     * Generates new token and returns its serialized string representation
     * @return string
     */
    public function generateNewToken(): string;
}