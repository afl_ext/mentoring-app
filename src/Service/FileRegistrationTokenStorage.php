<?php
declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;

/**
 * Implements token storage interface using files as token storage
 */
class FileRegistrationTokenStorage implements RegistrationTokenStorageInterface
{
    /**
     * @var TimeProviderInterface
     */
    private $timeProvider;
    /**
     * @var string
     */
    private $storagePath;
    /**
     * @var Filesystem
     */
    private $fileSystem;


    /**
     * @param TimeProviderInterface $timeProvider
     * @param Filesystem $fileSystem
     * @param string $storagePath
     */
    public function __construct(TimeProviderInterface $timeProvider, Filesystem $fileSystem, string $storagePath)
    {
        $this->timeProvider = $timeProvider;
        $this->storagePath = $storagePath;
        $this->fileSystem = $fileSystem;
    }

    /**
     * @param string $token
     * @return string
     */
    private function getPathForToken(string $token): string
    {
        return $this->storagePath . md5($token);
    }

    /**
     * @param string $token
     */
    public function saveRegistrationToken(string $token): void
    {
        $contents = $token . "\n" . $this->timeProvider->getUnixTimestamp();
        $path = $this->getPathForToken($token);
        $this->fileSystem->dumpFile($path, $contents);
    }

    /**
     * @param string $token
     * @return bool
     */
    public function isActiveToken(string $token): bool
    {
        $path = $this->getPathForToken($token);
        return $this->fileSystem->exists($path);
    }

    /**
     * @param string $token
     */
    public function deactivateToken(string $token): void
    {
        $path = $this->getPathForToken($token);
        if ($this->isActiveToken($token)) {
            $this->fileSystem->remove($path);
        }
    }

}
