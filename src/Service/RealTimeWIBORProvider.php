<?php

namespace App\Service;


class RealTimeWIBORProvider implements WIBORProviderInterface
{

    // TODO do it better
    public function getWIBOR(): float
    {
        $ch = curl_init ('https://stooq.pl/q/a2/d/?s=ploplnon&i=d');
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_ENCODING, 'identity');
        $rawData=curl_exec($ch);
        curl_close ($ch);
        $csvLines = explode("\n", $rawData);
        $arrayData = str_getcsv($csvLines[count($csvLines) - 2]);
        return $arrayData[2];
    }
}