<?php
declare(strict_types=1);

namespace App\Service;


/**
 * Provides configuration for RegistrationTokenLimitChecker
 */
interface RegistrationTokenLimitConfigurationProviderInterface
{
    /**
     * @return int
     */
    public function getCounterValue(): int;

    public function incrementCounterValue(): void;

    public function resetCounterValue(): void;

    /**
     * Gets the time after which token generation will be re-enabled
     * @return int
     */
    public function getAllowAfterTime(): int;

    /**
     * Sets the time when token generation will be re-enabled
     * @param int $timestamp
     */
    public function setAllowAfterTime(int $timestamp): void;
}