<?php
declare(strict_types=1);

namespace App\Service;


/**
 * Interface for checking how many tokens were generated since midnight
 */
interface RegistrationTokenCountPerDayProviderInterface
{
    /**
     * Gets the count of tokens generated since current day midnight
     * @return int
     */
    public function getTokenCountGeneratedToday(): int;

    /**
     * Registers new token that causes counter increment
     */
    public function registerNewToken(): void;
}