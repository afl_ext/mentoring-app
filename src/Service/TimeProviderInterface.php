<?php
declare(strict_types=1);

namespace App\Service;

/**
 * Interface for time provider that will allow mocking up the time when needed
 */
interface TimeProviderInterface
{
    /**
     * @return int
     */
    public function getUnixTimestamp(): int;

    /**
     * @param string $format
     * @return string
     */
    public function getFormattedDateTimeString(string $format): string;
}