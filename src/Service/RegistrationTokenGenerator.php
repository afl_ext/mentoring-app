<?php
declare(strict_types=1);
namespace App\Service;

use App\Entity\RegistrationToken;
use App\Repository\ConfigurationEntryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class RegistrationTokenGenerator implements RegistrationTokenGeneratorInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ConfigurationEntryRepository
     */
    private $translator;

    /**
     * @var RegistrationTokenLimitCheckerInterface
     */
    private $tokenLimitChecker;
    /**
     * @var RegistrationTokenStringGeneratorInterface
     */
    private $tokenStringGenerator;

    /**
     * @var RegistrationTokenStorageInterface
     */
    private $registrationTokenStorage;

    /**
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface $translator
     * @param RegistrationTokenLimitCheckerInterface $limitChecker
     * @param RegistrationTokenStringGeneratorInterface $tokenStringGenerator
     * @param RegistrationTokenStorageInterface $registrationTokenStorage
     */
    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator,
                                RegistrationTokenLimitCheckerInterface $limitChecker,
                                RegistrationTokenStringGeneratorInterface $tokenStringGenerator,
                                RegistrationTokenStorageInterface $registrationTokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
        $this->tokenLimitChecker = $limitChecker;
        $this->tokenStringGenerator = $tokenStringGenerator;
        $this->registrationTokenStorage = $registrationTokenStorage;
    }

    /**
     * Generates new registration token in following way:
     * - Checks if token limit rules are met, if yes continue, if not throw RegistrationTokenLimitExceededException
     * - Generates new token and registers it in limit checker
     * - returns new token serialized string representation
     * @return string
     * @throws \App\Exception\RegistrationTokenLimitExceededException
     */
    public function generateNewToken(): string
    {
        $this->tokenLimitChecker->throwIfNewTokenNotAllowed();

        $tokenString = $this->tokenStringGenerator->generate();
        $this->registrationTokenStorage->saveRegistrationToken($tokenString);
        $this->tokenLimitChecker->registerNewToken();
        return $tokenString;
    }
}