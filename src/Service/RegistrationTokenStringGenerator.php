<?php
declare(strict_types=1);
namespace App\Service;


use Ramsey\Uuid\Uuid;


class RegistrationTokenStringGenerator implements RegistrationTokenStringGeneratorInterface
{
    /**
     * @var RegistrationTokenCountPerDayProviderInterface
     */
    private $tokenCountPerDayProvider;

    /**
     * @var TimeProviderInterface
     */
    private $timeProvider;

    /**
     * @param RegistrationTokenCountPerDayProviderInterface $tokenCountPerDayProvider
     * @param TimeProviderInterface $timeProvider
     */
    public function __construct(RegistrationTokenCountPerDayProviderInterface $tokenCountPerDayProvider, TimeProviderInterface $timeProvider)
    {
        $this->tokenCountPerDayProvider = $tokenCountPerDayProvider;
        $this->timeProvider = $timeProvider;
    }

    /**
     * @return string
     */
    public function generate(): string
    {
        $countToday = $this->tokenCountPerDayProvider->getTokenCountGeneratedToday();
        $countToday = $countToday < 10 ? ('0'.$countToday) : (''.$countToday);

        $secretPhase = (int)$this->timeProvider->getFormattedDateTimeString("H") < 12 ? "gd’ mornin’" : 'hail from sky';
        $tokenStart = $countToday.'-'.$this->timeProvider->getFormattedDateTimeString('Y-m/d-H:i').'('.$secretPhase.')->';
        $guid = Uuid::uuid4()->toString();
        // ltrim to avoid accidental cast from octet integer representation
        $cutter = (int)ltrim($this->timeProvider->getFormattedDateTimeString('i'), '0')
            % (int)$this->timeProvider->getFormattedDateTimeString('G');
        $guid = substr($guid, 0, $cutter);
        return $tokenStart.$guid;
    }
}