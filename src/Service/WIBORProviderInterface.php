<?php

namespace App\Service;


interface WIBORProviderInterface
{
    public function getWIBOR(): float;
}