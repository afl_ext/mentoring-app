<?php
declare(strict_types=1);
namespace App\Service;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Interface for serializing tokens string representations to string and deserialize from string
 */
interface TokenSerializerInterface extends DataTransformerInterface
{
    /**
     * Serializes a string
     * @param string $raw_representation string to be serialized
     * @return string
     */
    public function transform($raw_representation): ?string;

    /**
     * De-serializes a string
     * @param string $serialized_representation serialized string representation
     * @return string
     */
    public function reverseTransform($serialized_representation): string;
}