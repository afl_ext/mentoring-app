<?php
declare(strict_types=1);
namespace App\Tests;

use App\Service\FileRegistrationTokenStorage;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use App\Tests\Mocks\MockingTimeProvider;
use Symfony\Component\Filesystem\Filesystem;

class FileRegistrationTokenStorageTest extends TestCase
{
    /**
     * @return FileRegistrationTokenStorage
     */
    private function createTokenStorage(): FileRegistrationTokenStorage
    {
        $timeMock = new MockingTimeProvider();
        $fileSystem = new Filesystem();
        return new FileRegistrationTokenStorage($timeMock, $fileSystem, sys_get_temp_dir());
    }

    /**
     * Tests if a created token is active
     */
    public function testIfSavedTokenIsActive(): void
    {
        $storage = $this->createTokenStorage();
        $storage->saveRegistrationToken("TOKEN");
        Assert::assertTrue($storage->isActiveToken("TOKEN"));
    }

    /**
     * Test if a created and deleted token is not active
     */
    public function testIfRemovedTokenIsNotActive(): void
    {
        $storage = $this->createTokenStorage();
        $storage->saveRegistrationToken("TOKEN");
        Assert::assertTrue($storage->isActiveToken("TOKEN"));
        $storage->deactivateToken("TOKEN");
        Assert::assertFalse($storage->isActiveToken("TOKEN"));
    }

    /**
     * Test if invalid token is not active
     */
    public function testIfInvalidTokenIsNotActive(): void
    {
        $storage = $this->createTokenStorage();
        Assert::assertFalse($storage->isActiveToken("INVALID"));
    }
}