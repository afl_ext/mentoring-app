<?php
//declare(strict_types=1);
namespace App\Tests\Mocks;


use Symfony\Component\Translation\TranslatorInterface;

/**
 * Empty translator stub to make unit testing easier
 */
class MockingEmptyTranslator implements TranslatorInterface
{

    /**
     * @return string empty string
     */
    public function trans($id, array $parameters = array(), $domain = null, $locale = null)
    {
        return "";
    }

    /**
     * @return string empty string
     */
    public function transChoice($id, $number, array $parameters = array(), $domain = null, $locale = null)
    {
        return "";
    }

    /**
     */
    public function setLocale($locale)
    {
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return 'en';
    }
}