<?php
declare(strict_types=1);

namespace App\Tests\Mocks;


use App\Service\RegistrationTokenLimitConfigurationProviderInterface;

/**
 * Implements a mocking class for RegistrationTokenLimitConfigurationProviderInterface
 * This uses internal variables, data is not preserved
 */
class MockingInMemoryRegistrationTokenLimitConfigurationProvider implements RegistrationTokenLimitConfigurationProviderInterface
{
    /**
     * @var int
     */
    private $counter;
    /**
     * @var int
     */
    private $allowAfterTime;

    /**
     * MockingInMemoryRegistrationTokenLimitConfigurationProvider constructor.
     */
    public function __construct()
    {
        $this->counter = 0;
        $this->allowAfterTime = 0;
    }

    /**
     * @return int
     */
    public function getCounterValue(): int
    {
        return $this->counter;
    }

    /**
     *
     */
    public function incrementCounterValue(): void
    {
        $this->counter++;
    }

    /**
     *
     */
    public function resetCounterValue(): void
    {
        $this->counter = 0;
    }

    /**
     * @return int
     */
    public function getAllowAfterTime(): int
    {
        return $this->allowAfterTime;
    }

    /**
     * @param int $timestamp
     */
    public function setAllowAfterTime(int $timestamp): void
    {
        $this->allowAfterTime = $timestamp;
    }
}