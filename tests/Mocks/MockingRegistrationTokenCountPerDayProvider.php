<?php
declare(strict_types=1);

namespace App\Tests\Mocks;


use App\Service\RegistrationTokenCountPerDayProviderInterface;

/**
 *
 */
class MockingRegistrationTokenCountPerDayProvider implements RegistrationTokenCountPerDayProviderInterface
{

    /**
     * Returns 0 - this count is not important for tests
     * @return int
     */
    public function getTokenCountGeneratedToday(): int
    {
        return 0;
    }

    /**
     * Does nothing
     */
    public function registerNewToken(): void
    {
    }
}