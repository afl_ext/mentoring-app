<?php
declare(strict_types=1);

namespace App\Tests\Mocks;

use App\Service\TimeProviderInterface;


/**
 * Provides real time with additional possibility of fast-forwarding and rewinding time
 */
class MockingTimeProvider implements TimeProviderInterface
{

    /**
     * @var int
     */
    private $timeOffset = 0;

    /**
     * @return int
     */
    public function getUnixTimestamp(): int
    {
        return (new \DateTime())->getTimestamp() + $this->timeOffset;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getFormattedDateTimeString(string $format): string
    {
        return date($format, $this->getUnixTimestamp());
    }

    /**
     * @param int $secAmount
     */
    public function setTimeOffset(int $secAmount): void
    {
        $this->timeOffset = $secAmount;
    }

    /**
     * @param int $secAmount
     */
    public function fastForwardTime(int $secAmount): void
    {
        $this->timeOffset += $secAmount;
    }

    /**
     * @param int $secAmount
     */
    public function rewind(int $secAmount): void
    {
        $this->timeOffset -= $secAmount;
    }

    /**
     * @return int
     */
    public function getTimeOffset(): int
    {
        return $this->timeOffset;
    }
}