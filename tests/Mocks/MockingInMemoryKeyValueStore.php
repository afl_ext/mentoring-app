<?php
declare(strict_types=1);
namespace App\Tests\Mocks;


use App\Service\KeyValueStoreInterface;

/**
 * Holds the key value store in memory without persisting it anywhere
 */
class MockingInMemoryKeyValueStore implements KeyValueStoreInterface
{

    /**
     * Holds in memory key value store
     * @var array
     */
    private $data = [];

    /**
     * @param string $key
     * @return null|string
     */
    public function getValueByKey(string $key): ?string
    {
        return array_key_exists($key, $this->data) ? $this->data[$key] : null;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function setValueByKey(string $key, string $value): void
    {
        $this->data[$key] = $value;
    }
}