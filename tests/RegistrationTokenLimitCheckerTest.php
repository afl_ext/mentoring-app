<?php
declare(strict_types=1);
namespace App\Tests;

use App\Exception\RegistrationTokenLimitExceededException;
use App\Tests\Mocks\MockingInMemoryRegistrationTokenLimitConfigurationProvider;
use App\Service\RegistrationTokenLimitChecker;
use App\Service\RegistrationTokenLimitCheckerInterface;
use App\Tests\Mocks\MockingRegistrationTokenCountPerDayProvider;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use App\Tests\Mocks\MockingTimeProvider;

class RegistrationTokenLimitCheckerTest extends TestCase
{

    private const CONFIG_TOKEN_COUNT_LIMIT = 5;
    private const CONFIG_TOKEN_TIMEOUT = 3600;
    private $mockedTime;

    private function createLimitChecker($tokenCountLimit, $tokenTimeout): RegistrationTokenLimitCheckerInterface
    {
        $this->mockedTime = new MockingTimeProvider();
        $mockingConfigurationProvider = new MockingInMemoryRegistrationTokenLimitConfigurationProvider();
        $mockingCountPerDayProvider = new MockingRegistrationTokenCountPerDayProvider();

        return new RegistrationTokenLimitChecker($mockingConfigurationProvider, $this->mockedTime, $mockingCountPerDayProvider,
            $tokenCountLimit, $tokenTimeout);
    }

    /**
     * Tests if limit checker will not throw when limit is not exceeded
     */
    public function testIfWontThrowUnderLimits(): void
    {
        $limitChecker = $this->createLimitChecker(static::CONFIG_TOKEN_COUNT_LIMIT, static::CONFIG_TOKEN_TIMEOUT);
        $tokens = 0;
        for(;$tokens < static::CONFIG_TOKEN_COUNT_LIMIT; $tokens++){
            $limitChecker->throwIfNewTokenNotAllowed();
            $limitChecker->registerNewToken();
        }
        Assert::assertEquals($tokens, static::CONFIG_TOKEN_COUNT_LIMIT); // I did not found Assert exception not thrown
    }

    /**
     * Tests if limit checker will throw when exceeding count limit
     */
    public function testIfThrowExceedingLimits():void
    {
        $limitChecker = $this->createLimitChecker(static::CONFIG_TOKEN_COUNT_LIMIT, static::CONFIG_TOKEN_TIMEOUT);
        $tokens = 0;
        $this->expectException(RegistrationTokenLimitExceededException::class);
        for(;$tokens < static::CONFIG_TOKEN_COUNT_LIMIT + 1; $tokens++){
            $limitChecker->throwIfNewTokenNotAllowed();
            $limitChecker->registerNewToken();
        }
    }

    /**
     * Tests if the limit checker will not throw if timeout passed
     */
    public function testIfTimeoutResetsLimit():void
    {
        $limitChecker = $this->createLimitChecker(static::CONFIG_TOKEN_COUNT_LIMIT, static::CONFIG_TOKEN_TIMEOUT);
        $tokens = 0;
        for(;$tokens < static::CONFIG_TOKEN_COUNT_LIMIT; $tokens++){
            $limitChecker->throwIfNewTokenNotAllowed();
            $limitChecker->registerNewToken();
        }
        $this->mockedTime->fastForwardTime(static::CONFIG_TOKEN_TIMEOUT+1);
        $limitChecker->throwIfNewTokenNotAllowed();
        $limitChecker->registerNewToken();
        Assert::assertTrue(true);
    }

    /**
     * Tests if the limit checker will throw if timeout did not passed before trying
     */
    public function testIfThrowsBeforeTimeout():void
    {
        $limitChecker = $this->createLimitChecker(static::CONFIG_TOKEN_COUNT_LIMIT, static::CONFIG_TOKEN_TIMEOUT);
        $tokens = 0;
        for(;$tokens < static::CONFIG_TOKEN_COUNT_LIMIT; $tokens++){
            $limitChecker->throwIfNewTokenNotAllowed();
            $limitChecker->registerNewToken();
        }
        $this->mockedTime->fastForwardTime(static::CONFIG_TOKEN_TIMEOUT);
        $this->expectException(RegistrationTokenLimitExceededException::class);
        $limitChecker->throwIfNewTokenNotAllowed();
    }
}